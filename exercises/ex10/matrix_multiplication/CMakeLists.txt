cmake_minimum_required (VERSION 3.15)
project (ex10-matrix-multiplication)

set (CMAKE_CXX_STANDARD 17)
set (CMAKE_CXX_STANDARD_REQUIRED TRUE)
set (CMAKE_CXX_EXTENSIONS FALSE)

add_compile_options (-Wall -Wextra -Wpedantic -march=native)

add_executable (main main.cpp mm0.cpp)
