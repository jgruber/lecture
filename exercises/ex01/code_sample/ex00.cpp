#include <vector>

// Note: this would be a good use case for templates
// Sort range [start, end]
void mergeSort(std::vector<int>& data, int start, int end) {
    const int middle = (start + end) / 2;

    // TODO: Insert some termination condition.
    mergeSort(data, start, middle);
    mergeSort(middle + 1, end);

    // TODO: implement. Possibly modify signature to use a
    // single allocation of auxiliary memory
    merge(data, start, middle, end);
}

void mergeSort(std::vector<int>& data){
    return mergeSort(data, 0, data.size() - 1);
}

int main(){
    // TODO: Test here or use LeetCode.
    return 0;
}
