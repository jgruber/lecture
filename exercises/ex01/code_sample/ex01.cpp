#include <iostream>
#include <cmath>
#include <functional>

double integrate(const std::function<double(double)>& f, const double x0, const double xf, const int n_intervals) {
    // Your implementation goes here:
    return 0;
}

int main(){
    // Improve the test.
    auto f = [](double x){return std::sin(x); };

    // You can also declare the function as
    // auto f = (double(*)(double))& std::sin;

    const double res = integrate(f, 0,  M_PI / 3, 10);

    const auto diff = std::abs(res - std::cos(M_PI/ 3));

    std::cout << (diff < 1e-5 ? "SUCCESS" : "FAILURE") << std::endl;
}