# Abort on error (works only on bash and bash-compatible shells).
set -e

# the option -S requires cmake 3.13; for prior cmake, use -H
export INSTALL_LOCATION="${PWD}/install"

mkdir -p "${INSTALL_LOCATION}/lib"
mkdir -p "${INSTALL_LOCATION}/include"

# Install timer library
cmake -Stimer -Btimer/build -DCMAKE_INSTALL_PREFIX="${INSTALL_LOCATION}" -DCMAKE_BUILD_TYPE=Release
make -C timer/build
make -C timer/build install

# Install random library
cmake -Srandom -Brandom/build -DCMAKE_INSTALL_PREFIX="${INSTALL_LOCATION}" -DCMAKE_BUILD_TYPE=Release
make -C random/build
make -C random/build install

# Compile benchmark
cmake -Sbenchmark -Bbenchmark/build -DCMAKE_INSTALL_PREFIX="${INSTALL_LOCATION}" -DCMAKE_BUILD_TYPE=Release
make -C benchmark/build
