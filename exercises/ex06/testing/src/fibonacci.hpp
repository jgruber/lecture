#ifndef FIBONACCI_HPP
#define FIBONACCI_HPP

#include <cstdint>

uint64_t fibonacci(uint32_t n);

#endif  // FIBONACCI_HPP
