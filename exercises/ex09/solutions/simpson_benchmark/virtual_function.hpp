/*
 * Programming Techniques for Scientific Simulations I
 * HS 2019
 * Exercise 9.1
 */

#pragma once

class VirtualFunction {
	public:
		virtual ~VirtualFunction() = default;
		virtual double operator()(double const x) const noexcept = 0;
};
