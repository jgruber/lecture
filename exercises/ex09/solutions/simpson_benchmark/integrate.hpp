/*
 * Programming Techniques for Scientific Simulations I
 * HS 2019
 * Exercise 9.1
 */

#pragma once
#include <cassert> // for assert
#include <cstddef> // for std::size_t
#include <cmath> // for std::sin
#include "virtual_function.hpp"

#define INTEGRATION_BODY(f, a, b, bins) do { \
	assert(bins != 0); \
\
	double const dr = (b - a) / (2 * bins); \
	double I2 = 0; \
	double I4 = 0; \
	double x = a; \
\
	for (std::size_t i = 0; i < bins; ++i) { \
		x += dr; \
		I4 += f(x); \
		x += dr; \
		I2 += f(x); \
	}\
\
	return (dr / 3) * (f(a) + 2 * I2 + 4 * I4 - f(b)); \
} while (false)

// In order to keep code duplication to a minimum, use X-Macros
// https://en.wikibooks.org/wiki/C_Programming/Preprocessor_directives_and_macros#X-Macros
#define F(x) 0
#define FNAME f1
#include "integrate.def"
#undef F
#undef FNAME

#define F(x) 1
#define FNAME f2
#include "integrate.def"
#undef F
#undef FNAME

#define F(x) x
#define FNAME f3
#include "integrate.def"
#undef F
#undef FNAME

#define F(x) x * x
#define FNAME f4
#include "integrate.def"
#undef F
#undef FNAME

#define F(x) std::sin(x)
#define FNAME f5
#include "integrate.def"
#undef F
#undef FNAME

#define F(x) std::sin(5 * x)
#define FNAME f6
#include "integrate.def"
#undef F
#undef FNAME

/// Integrate a function passed as pointer
///
/// @param f    function to integrate
/// @param a    lower integration bound
/// @param b    upper integration bound
/// @param bins number of bins
///
/// @returns \f$ \int_{a}^{b} f(x) \dd x \f$
///
/// @pre bins > 0
///
/// @see https://en.wikipedia.org/wiki/Simpson%27s_rule
double integrate_pointer(double (*f)(double const), double const a, double const b, std::size_t const bins) noexcept {
	INTEGRATION_BODY(f, a, b, bins);
}

/// Integrate a non-virtual function object
///
/// @param f    function to integrate
/// @param a    lower integration bound
/// @param b    upper integration bound
/// @param bins number of bins
///
/// @returns \f$ \int_{a}^{b} f(x) \dd x \f$
///
/// @pre bins > 0
///
/// @see https://en.wikipedia.org/wiki/Simpson%27s_rule
template<class F>
double integrate_template(F f, double const a, double const b, std::size_t const bins) noexcept {
	INTEGRATION_BODY(f, a, b, bins);
}

/// Integrate a virtual function
///
/// @param f    function to integrate
/// @param a    lower integration bound
/// @param b    upper integration bound
/// @param bins number of bins
///
/// @returns \f$ \int_{a}^{b} f(x) \dd x \f$
///
/// @pre bins > 0
///
/// @see https://en.wikipedia.org/wiki/Simpson%27s_rule
double integrate_virtual(VirtualFunction const &f, double const a, double const b, std::size_t const bins) noexcept {
	INTEGRATION_BODY(f, a, b, bins);
}
