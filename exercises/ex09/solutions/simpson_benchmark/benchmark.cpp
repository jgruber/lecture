/*
 * Programming Techniques for Scientific Simulations I
 * HS 2019
 * Exercise 9.1
 */

#include <chrono>
#include <cstdlib> // for std::abort
#include <iostream>
#include "integrate.hpp"

/// Time a function
///
/// @param f function to time
/// @returns amount of seconds f took to run
template<class F>
double time(F f) noexcept {
	auto const start = std::chrono::high_resolution_clock::now();
	// save the result of f in a volatile variable, to stop the compiler from optimizing the call away
	[[maybe_unused]] auto volatile result = f();
	auto const end = std::chrono::high_resolution_clock::now();
	std::chrono::duration<double> const time = end - start;
	return time.count();
}

#define CONCAT(x, y) CONCAT2(x, y)
#define CONCAT2(x, y) x ## y

#define STRINGIFY(x) STRINGIFY2(x)
#define STRINGIFY2(x) #x

#define TEST(FNAME, a, b, bins, exact, tolerance) do { \
	bool failed = false; \
	auto result = CONCAT(integrate_, FNAME)(a, b, bins); \
	auto difference = exact - result; \
	if (std::abs(difference) > tolerance) { \
		failed = true; \
		std::cerr << "integrate_" << STRINGIFY(FNAME) << " gave wrong results: " << result << " vs " << exact << " diff " << difference << '\n'; \
	} \
\
	result = integrate_pointer(FNAME, a, b, bins); \
	difference = exact - result; \
	if (std::abs(difference) > tolerance) { \
		failed = true; \
		std::cerr << "integrate_pointer(" << STRINGIFY(FNAME) << ", ...) gave wrong results: " << result << " vs " << exact << " diff " << difference << '\n'; \
	} \
\
	result = integrate_template(FNAME, a, b, bins); \
	difference = exact - result; \
	if (std::abs(difference) > tolerance) { \
		failed = true; \
		std::cerr << "integrate_template(" << STRINGIFY(FNAME) << ", ...) gave wrong results: " << result << " vs " << exact << " diff " << difference << '\n'; \
	} \
\
	result = integrate_virtual(CONCAT(virtual_, FNAME)(), a, b, bins); \
	difference = exact - result; \
	if (std::abs(difference) > tolerance) { \
		failed = true; \
		std::cerr << "integrate_virtual(" << STRINGIFY(FNAME) << ", ...) gave wrong results: " << result << " vs " << exact << " diff " << difference << '\n'; \
	} \
\
	if (failed) { \
		std::abort(); \
	} \
} while (false)

#define BENCHMARK(FNAME, runs, a, b, bins) do { \
	for (int i = 0; i < runs; ++i) { \
		std::cout << #FNAME << ',' << "hardcoded" << ',' << time([](){ return CONCAT(integrate_, FNAME)(                   a, b, bins); }) << '\n'; \
		std::cout << #FNAME << ',' << "pointer"   << ',' << time([](){ return integrate_pointer(FNAME,                     a, b, bins);}) << '\n'; \
		std::cout << #FNAME << ',' << "template"  << ',' << time([](){ return integrate_template(FNAME,                    a, b, bins);}) << '\n'; \
		std::cout << #FNAME << ',' << "virtual"   << ',' << time([](){ return integrate_virtual(CONCAT(virtual_, FNAME)(), a, b, bins); }) << '\n'; \
	} \
} while (false)

int main() {
	constexpr double tolerance = 1e-9;
	TEST(f1, 0, 1, 1024, 0,               tolerance);
	TEST(f2, 0, 1, 1024, 1,               tolerance);
	TEST(f3, 0, 1, 1024, 0.5,             tolerance);
	TEST(f4, 0, 1, 1024, 1.0 / 3.0,       tolerance);
	TEST(f5, 0, 1, 1024, 1 - std::cos(1), tolerance);
	TEST(f6, 0, 1, 1024, 0.1432675629,    tolerance);

	std::cout << "function,type,time\n";
	BENCHMARK(f1, 100, 0, 1, 1024 * 1024);
	BENCHMARK(f2, 100, 0, 1, 1024 * 1024);
	BENCHMARK(f3, 100, 0, 1, 1024 * 1024);
	BENCHMARK(f4, 100, 0, 1, 1024 * 1024);
	BENCHMARK(f5, 100, 0, 1, 1024 * 1024);
	BENCHMARK(f6, 100, 0, 1, 1024 * 1024);
}
