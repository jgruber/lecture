#!/usr/bin/env python3

# Programming Techniques for Scientific Simulations I
# HS 2019
# Exercise 9.2

import sys
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
sns.set()
sns.set_context("talk")

def main():
    if not (2 <= len(sys.argv) and len(sys.argv) <= 3):
        print('Usage: plot.py input_file [output_file]')
        exit(1)

    input_file = sys.argv[1]
    if len(sys.argv) == 3:
        output_file = sys.argv[2]

    data = pd.read_csv(input_file)
    data['ops'] = data['size'] / data['step']
    data['time'] *= 1e9

    plt.figure(figsize=(11.69,8.27))

    small = data.query('step <= 256')
    ax1 = plt.subplot(1, 2, 1)
    sns.lineplot(x='size', y='time', hue='step', data=small, ax=ax1, palette=sns.color_palette(n_colors=small['step'].nunique()))
    plt.gca().set_xscale('log', basex=2)
    plt.axvline(32 * 1024)
    plt.axvline(512 * 1024)
    plt.axvline(4 * 1024 * 1024)
    plt.title('Cache sizes', y=1.08)
    plt.xlabel('Size in bytes')
    plt.ylabel('Time in nanoseconds', rotation=0, horizontalalignment='left')
    plt.gca().yaxis.set_label_coords(0, 1)

    large = data.query('step >= 256')
    ax2 = plt.subplot(1, 2, 2)
    sns.lineplot(x='ops', y='time', hue='step', data=large, ax=ax2,  palette=sns.color_palette(n_colors=large['step'].nunique()))
    plt.gca().set_xscale('log', basex=2)
    plt.axvline(8)
    plt.title('Cache associativity', y=1.08)
    plt.xlabel('Size / step')
    plt.ylabel('Time in nanoseconds', rotation=0, horizontalalignment='left')
    plt.gca().yaxis.set_label_coords(0, 1)

    if len(sys.argv) == 3:
        plt.savefig(output_file, bbox_inches='tight')
    else:
        plt.show()


if __name__ == '__main__':
    main()
