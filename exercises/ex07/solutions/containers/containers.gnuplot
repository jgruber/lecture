# Programming Techniques for Scientific Simulations, HS 2019

set title 'containers benchmarks'
set xlabel 'n'
set ylabel 'time [ns]'

set log xy
set grid
set key top left

set terminal png
set output 'containers.png'

plot 'containers.dat' u 1:2 w lp ti 'vector', 'containers.dat' u 1:3 w lp ti 'list', 'containers.dat' u 1:4 w lp ti 'set'
