#include "sarray.hpp"
#include <iostream>

int main() {

  // create SArray with five elements
  SArray<int> a(5);

  // set some values (using index access!)
  for (int i=0; i < a.size(); ++i) {
    a[i] = i;
  }

    // print the array (using pointers)
  std::cout << "a = ";
  for (int* p=&a[0]; p!=&a[0]+a.size(); ++p) {
    std::cout << *p << " ";
  }
  std::cout << '\n';

  // print the array using our iterator
  std::cout << "a = ";
  for (SArray<int>::iterator p=a.begin(); p!=a.end(); ++p) {
    std::cout << *p << " ";
  }
  std::cout << '\n';

  // even more convenient: print array using range-based for
  std::cout << "a = ";
  for (auto const& elem : a) {
    std::cout << elem << " ";
  }
  std::cout << '\n';


}

