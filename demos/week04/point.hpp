#ifndef POINT_HPP
#define POINT_HPP

class Point {
  private:
    double x_,y_; // Cartesian coordinates
  public:
    // constructor
    Point(double x=0, double y=0); // defaults to (0,0)
    // Point(const Point&) = default; // C++11
    // destructor
    // ~Point() = default; // C++11
    // operators
    // Point& operator=(const Point&) = default; // C++11
    // further properties & operations
    double x() const;                // x coordinate
    double y() const;                // y coordinate
    double abs() const;              // distance from origin aka polar radius
    double dist(const Point&) const; // distance of this point to another point
    double angle() const;            // polar angle
    void invert() ;                  // mirror a point at origin (0,0)
};

#endif /* POINT_HPP */
