import numpy as np
import h5py

# create & open a new file
f = h5py.File("file.h5", "w")

# create a dataset
dset = f.create_dataset(name="dset", shape=(6,4), dtype='int32')

# write the dataset
dset[:,:] = [[6*i + j for i in range(4)] for j in range(6)]

# create a group
grp = f.create_group("somegroup")

# create & write a dataset to the just created group
grp.create_dataset(name="dset2", data=12) # h5py guesses shape, type, ...!!!

# close the file
f.close()

