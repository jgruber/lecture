cmake_minimum_required(VERSION 2.8)

# C++11
set(CMAKE_CXX_STANDARD 11)

# setting warning compiler flags
if(CMAKE_CXX_COMPILER_ID MATCHES "(C|c?)lang")
  add_compile_options(-Weverything)
else()
  add_compile_options(-Wall -Wextra -Wpedantic)
endif()

add_executable(stack_procedural stack_procedural.cpp)
add_executable(stack_modular stack_modular.cpp)
add_executable(stack_oo stack_oo.cpp)
add_executable(stack_generic stack_generic.cpp)

