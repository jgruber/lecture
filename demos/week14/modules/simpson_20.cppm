export module integrate;
#include <cstddef>

export using function_t = double(*)(double);
export double integrate(function_t f, double const a, double const b, std::size_t n);
