#include <cmath>
#include <iostream>
import integrate;

int main() {
	std::cout << integrate([](double x) { return std::sin(x); }, 0, 1, 100) << '\n';
}
