module integrate;

double integrate(function_t f, double a, double b, std::size_t n) {
	double dx = (b - a) / n;
	double result = 0;
	for (double x = a; x < b; x += dx) {
		result += dx / 6.0 * (f(x) + 4 * f(x + dx / 2.0) + f(x + dx));
	}
	return result;
}
