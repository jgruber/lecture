#pragma once
#include <cstddef> // for std::size_t

using function_t = double(*)(double);
double integrate(function_t f, double const a, double const b, std::size_t n);
