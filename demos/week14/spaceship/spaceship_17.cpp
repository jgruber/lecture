// C++17
struct String17 {
	static constexpr int N = 17;
	char s[N];

	bool operator==(String17 const & other) const noexcept {
		for (int i = 0; i < N; ++i) {
			if (s[i] != other.s[i]) {
				return false;
			}
		}
		return true;
	}

	bool operator<(String17 const & other) const noexcept {
		for (int i = 0; i < N; ++i) {
			if (s[i] < other.s[i]) {
				return true;
			}
		}
		return false;
	}

	bool operator!=(String17 const & other) const noexcept {
		return !(*this == other);
	}

	bool operator>(String17 const & other) const noexcept {
		return other < *this;
	}

	bool operator<=(String17 const & other) const noexcept {
		return !(*this > other);
	}

	bool operator>=(String17 const & other) const noexcept {
		return !(*this < other);
	}
};
